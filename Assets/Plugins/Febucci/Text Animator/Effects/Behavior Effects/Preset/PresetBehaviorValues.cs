﻿using UnityEngine;

namespace Febucci.UI.Core
{
    [System.Serializable]
    class PresetBehaviorValues : PresetBaseValues
    {

        [SerializeField] public EmissionControl emission;


        public PresetBehaviorValues() : base(false)
        {
            emission = new EmissionControl(false);
        }

        public override void Initialize()
        {
            base.Initialize();
            emission.Initialize(GetMaxDuration());

        }
    }

}