﻿using UnityEngine;

namespace Febucci.UI.Core
{
    [System.Serializable]
    class PresetBaseValues
    {
        public string effectTag;

        [SerializeField] public FloatCurve movementX;
        [SerializeField] public FloatCurve movementY;
        [SerializeField] public FloatCurve movementZ;

        [SerializeField] public FloatCurve scaleX;
        [SerializeField] public FloatCurve scaleY;

        [SerializeField] public FloatCurve rotX;
        [SerializeField] public FloatCurve rotY;
        [SerializeField] public FloatCurve rotZ;

        [SerializeField] public ColorCurve color;


        public PresetBaseValues(bool isAppearance)
        {
            int baseIdentifier = isAppearance ? 3 : 0;
            this.effectTag = string.Empty;

            movementX = new FloatCurve(baseIdentifier + 0, false);
            movementY = new FloatCurve(baseIdentifier + 0, false);
            movementZ = new FloatCurve(baseIdentifier + 0, false);

            scaleX = new FloatCurve(baseIdentifier + 1, false);
            scaleY = new FloatCurve(baseIdentifier + 1, false);

            rotX = new FloatCurve(baseIdentifier + 2, false);
            rotY = new FloatCurve(baseIdentifier + 2, false);
            rotZ = new FloatCurve(baseIdentifier + 2, false);

            color = new ColorCurve(false, isAppearance);
        }

        public float GetMaxDuration()
        {
            float GetEffectEvaluatorDuration(EffectEvaluator effect)
            {
                if (effect.isEnabled)
                    return effect.GetDuration();
                return 0;
            }

            return Mathf.Max
                (
                    GetEffectEvaluatorDuration(movementX),
                    GetEffectEvaluatorDuration(movementY),
                    GetEffectEvaluatorDuration(movementZ),
                    GetEffectEvaluatorDuration(scaleX),
                    GetEffectEvaluatorDuration(scaleY),
                    color.enabled ? color.GetDuration() : 0
                );
        }

        public virtual void Initialize()
        {
            movementX.Initialize();
            movementY.Initialize();
            movementZ.Initialize();

            scaleX.Initialize();
            scaleY.Initialize();

            rotX.Initialize();
            rotY.Initialize();
            rotZ.Initialize();

            color.Initialize();
        }
    }

}