﻿using UnityEngine;

namespace Febucci.UI.Core
{
    [System.Serializable]
    class PresetAppearanceValues : PresetBaseValues
    {
        public PresetAppearanceValues() : base(true)
        {

        }
    }
}