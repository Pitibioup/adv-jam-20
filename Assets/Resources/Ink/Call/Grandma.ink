(primary):  Hi grandma.

(secondary):    Hello sweetie, this is grandma speaking.

(primary):  I know, I'm the one calling you!

(primary): 
            * I wanted to check on you. 
			-> Desnouvelles
            * Mum told me to call. 
			->Maman


=== Desnouvelles ===
(secondary):    Oh, that is so nice of you!
(secondary):        How are you doing?
-> AboutYou


=== Maman ===
(secondary):    Alright... She called me last week.
(secondary):        How are you doing?
-> AboutYou


=== AboutYou ===
(primary):
            * [Talk about school] 
			->School
            * [Talk about your job] 
			->Job
            * [Talk about the routine]
			->Routine


=== School ===
(primary):  College's hard...
(primary):      ... we're working from home...
(primary):          ... so, it's a lot of distractions...
(primary):              ... we have classes all over the day until late...

(secondary):        Are the finals maintained?

(primary):  Yes, starting next week...

(secondary):    I'll pray for you.


->AboutGrandma


=== Job ===
(primary):  I've been laid off...
(primary):      ... my employer doesn't need me since we're quarantined...
(primary):          ... and he doesn't want to pay me...

[[fast]](secondary):    Normal. You're not working, you don't get paid.
(secondary):        That's the way it is.
->AboutGrandma


=== Routine ===
(primary):  Well, there's not much to say...
(primary):      ... I'm confined at home and don't go outside.
->AboutGrandma



=== AboutGrandma ===
(primary):  And you? How are you?

(secondary):    Fine, I went for a walk yesterday...

(primary): 
            * Getting some fresh air?
			->FreshAir
            * STAY AT HOME. 
			->STAYhome


=== FreshAir ===
(secondary):        Yes, I go everyday.
                        But yesterday, the streets were crowded, can you believe it?

[[fast]](primary):  I think I can yes...
->CountryStop

=== STAYhome ===
[[fast]](primary):  You should stay home, you're at risk.
->CountryStop


=== CountryStop ===
(secondary):    Oh, come on, that is nought but a flu!

[[fast]](primary):  Grandma, it's dangerous for you!

(secondary):    On the telly, they said it wasn't that dangerous.
(secondary):        We are overreacting with this lockdown.

(primary):
            * [Protest]
			->Protest
            * [Let her talk] 
			->LetHer

=== Protest ===
[[fast]](primary):  No, it is deadly and we don't have a cure yet.
[[fast]](primary):  Also, deseases won't stop while we're in lockdown, so we have to be careful.
[[destroy]](primary):  So be responsible and if not for yourself, stop putting people in danger! (secondary):    I won't stop living for it.
->CallEnd


=== LetHer ===
(secondary):    Last time, I wanted to buy stamp because I noticed I was short on stamps.
(secondary):        And the post office was closed!
(secondary):            Why? They don't even get exposed to people that much!
->CallEnd

=== CallEnd ===
(primary):
            * [Hang up.] 
			->END
            * [Goodbye.] 
			->Bye

=== Bye ===
(primary):  Ok, I have to go, grandma. Bye.
(secondary): Goodbye.
{ Desnouvelles and LetHer:
    It was nice of you to call.
    Pet the cat for me!
}
->END