-> story

=== story
(primary): GirlWaves
(secondary): SpockHand
(secondary): Television 
(secondary): GirlFace 
(secondary): Dragon
(secondary): Sword
(secondary): Fire
(primary): ManBeard
(primary): Wolf
(primary): InterrogationMark
(secondary): LolFace
(secondary): CrossArm
(secondary): Castle
(secondary): Mouse
(secondary): MusicNote
(primary):
        *  VomitFace
            (secondary):    CryingFace
        *  Fire
             (secondary):    HappyFace
 -
 (primary): Family
 (primary): InterrogationMark
 (secondary):   MehFace
 (secondary):   YouHand
 (primary):
        *   100Percent
            (secondary):    HappyFace
        *   MehFace 
            (secondary):    MehFace

[[1]](primary):
        *   $$1$$GrandmaFace
            [[1]](primary):  Telephone
            [[1]](secondary):   InterrogationMark
            [[1]](primary):
                **  $$1$$Walking
                    [[1]](primary): City
                    [[1]](secondary): AngryFace
                **  $$1$$ThumbsUp
                    [[1]](secondary): HappyFace
                **  $$1$$MehFace
                    [[1]](secondary): MehFace
-
(secondary): Television
(secondary): WaveHand
(primary):
        *   WaveHand ->DONE
        *   BlowKisse  ->DONE
        *   GirlWaves ->DONE