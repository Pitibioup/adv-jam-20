-> story

=== story
(primary): GirlWaves
(secondary): ExclamationMark
(secondary): InterrogationMark
(primary):
        *   HappyFace
            (secondary):    HappyFace
        *    Sushi
              (primary):    CryingFace
            (secondary):    LolFace
        *   Ice
            (primary):    Tea
            (secondary):    LolFace
 -
(secondary): Camera
(secondary): Peach
(secondary): InterrogationMark
(primary):
        *   RedSquare 
		->Period
        *   Eggplant 
            (secondary):    ThumbsUp 
            (primary):    Thirsty     
            ->Next
        *   RedX
            (secondary):    OkHand
            ->Next
 ===Period===
(primary):    Wave
(secondary):    DontCare
    (primary): 
                *    RedX
                        (secondary):    OkHand
                        (primary):    GrinFace
                        (secondary):    OkHand
                        ->Next
                *   GreenV
                        (secondary):    Thirsty
                        ->Next
=== Next ===
(primary):
        *   {Period}   Eggplant 
            (secondary):    ThumbsUp 
            (primary):    Thirsty
        *   YouHand 
            (primary):    InterrogationMark
            (secondary):    StrongArm
        *   WaveHand 
            (secondary):    BlowKisses
            ->DONE
-
(primary):
    *   Cage 
            (secondary):    LittleFingers
            (secondary):    StrongArm
            (primary):  ThumbsUp
                (primary):
                      **       WaveHand 
            (secondary):    BlowKisse 
            ->DONE
    *   WaveHand
            (secondary):    BlowKisse
            ->DONE