﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Nope : MonoBehaviour
{
    private float lifeTime = 7.0f;
    private Color col;

    void Start ()
    {
        Vector3 pos = gameObject.GetComponent<RectTransform>().localPosition;
        pos.x = 0.25f * gameObject.transform.parent.gameObject.GetComponent<RectTransform>().sizeDelta.x;
        pos.y = 0.25f * gameObject.transform.parent.gameObject.GetComponent<RectTransform>().sizeDelta.y;
        gameObject.GetComponent<RectTransform>().localPosition = pos;
    }

    void Update()
    {
        if (!GameManager.Instance.PhoneIsOpen())
        {
            Destroy(gameObject);
        }
        if (lifeTime>0)
        {
            col = transform.Find("NopeText").gameObject.GetComponent<Text>().color;
            col.a -= 0.001f;
            transform.Find("NopeText").gameObject.GetComponent<Text>().color = col;
            lifeTime -= Time.deltaTime;
        } else {
            Destroy(gameObject);
        }
    }
}

