﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Messenger_Selection : MonoBehaviour
{
    public UnityEngine.UI.Button Sister_Button;
    public UnityEngine.UI.Button Karim_Button;

    private GameManager gameManager;

    void Start()
    {
        gameManager = GameManager.Instance;

        Sister_Button.onClick.AddListener(() => gameManager.LancerAppliMessenger((int)0));
        Karim_Button.onClick.AddListener(() => gameManager.LancerAppliMessenger((int)1));
    }
}
