﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CallAccessibility : MonoBehaviour
{
    private int acessibilityFontSize = 37;

    void Awake ()
    {
        if (GameManager.Instance.getAccessibilitySize())
        {
            transform.Find("Text").gameObject.GetComponent<TMPro.TextMeshProUGUI>().fontSize = acessibilityFontSize;
        }
        if (GameManager.Instance.getAccessibilitySpeed())
        {
            gameObject.transform.Find("Text").gameObject.tag = "accessibility";
        }
    }

    void Start ()
    {
        if (!GameManager.Instance.getAccessibilityAnim())
        {
            transform.Find("Text").gameObject.GetComponent<TMPro.TextMeshProUGUI>().text = "<wave>" + transform.Find("Text").gameObject.GetComponent<TMPro.TextMeshProUGUI>().text;
        }
    }
}
