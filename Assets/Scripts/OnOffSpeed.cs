﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnOffSpeed : MonoBehaviour
{
    private GameManager gameManager;
    private GameObject On;
    private GameObject Off;

    void Start()
    {
        On = transform.parent.Find("On").gameObject;
        Off = transform.parent.Find("Off").gameObject;

        gameManager = GameManager.Instance;

        GetComponent<UnityEngine.UI.Button>().onClick.AddListener(() => swapOnOff());
    }

    private void swapOnOff()
    {
        bool accessibility = !gameManager.getAccessibilitySpeed();
        gameManager.setAccessibilitySpeed(accessibility);
        On.SetActive(accessibility);
        Off.SetActive(!accessibility);
    }
}
