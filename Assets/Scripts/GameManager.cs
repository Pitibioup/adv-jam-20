﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    private static GameManager _instance;
    public static GameManager Instance { get { return _instance; } }

    public GameObject telephone;
    public GameObject call_gameObj;
    public GameObject sms_gameObj;
    public GameObject messenger_gameObj;
    public GameObject album_gameObj;
    public GameObject mails_gameObj;
    public GameObject nope_prefab;
    public GameObject credits;

    private ButtonManager phone;
    private Call call;
    private SMS sms;
    private Messenger messenger;
    private Album album;
    private Mails mails;

    private bool talkedToGrandma;
    private bool accessibilityAnim;
    private bool accessibilitySize;
    private bool accessibilitySpeed;


    private void Awake()
    {
        // Singleton design pattern
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
        call_gameObj.SetActive(true);
        sms_gameObj.SetActive(true);
        messenger_gameObj.SetActive(true);
        album_gameObj.SetActive(true);
        mails_gameObj.SetActive(true);
        credits.SetActive(false);
    }

    void Start ()
    {
        phone = ButtonManager.Instance;
        call = Call.Instance;
        sms = SMS.Instance;
        messenger = Messenger.Instance;
        album = Album.Instance;
        mails = Mails.Instance;

        // Active le telephone et ferme les applis
        call_gameObj.SetActive(false);
        sms_gameObj.SetActive(false);
        messenger_gameObj.SetActive(false);
        album_gameObj.SetActive(false);
        mails_gameObj.SetActive(false);

        talkedToGrandma = false;
        accessibilityAnim = false;
        accessibilitySize = false;
        accessibilitySpeed = false;

        openTelephone();

        nopeMessage("Maybe I should check on people?");
    }

    private void playCarSound()
    {
        AudioSource sound = gameObject.GetComponent<AudioSource>();
        sound.Play();
    }

    public void nopeMessage (string message = "Nope")
    {
        GameObject obj = Instantiate(nope_prefab,telephone.transform.parent);
        obj.transform.Find("NopeText").gameObject.GetComponent<Text>().text = message;
    }

    public void LancerAppliTelephone (int index)
    {
        closeTelephone();
        call.ouvrirAppli(index);
    }

    public void LancerAppliSMS(int index)
    {
        if (!sms.convIsDone(index)) // haven't talked to that person yet
        {
            closeTelephone();
            sms.ouvrirAppli(index);
        } else
        {
            nopeMessage("I already talked to them today.");
        }

    }

    public void LancerAppliMessenger(int index)
    {
        if (!messenger.convIsDone(index)) // haven't talked to that person yet
        {
            closeTelephone();
            messenger.ouvrirAppli(index);
        }
        else
        {
            nopeMessage("I already talked to them today.");
        }
    }

    public void LancerAppliAlbum(int index)
    {
        closeTelephone();
        album.ouvrirAppli(index);
    }

    public void LancerAppliMails(int index)
    {
        closeTelephone();
        mails.ouvrirAppli(index);
    }

    public void FermerAppliTelephone()
    {
        call.fermerAppli();
        openTelephone();
    }

    public void FermerAppliSMS()
    {
        sms.fermerAppli();
        openTelephone();
    }

    public void FermerAppliMessenger()
    {
        messenger.fermerAppli();
        openTelephone();
    }

    public void FermerAppliAlbum()
    {
        album.fermerAppli();
        openTelephone();
    }

    public void FermerAppliMails()
    {
        mails.fermerAppli();
        openTelephone();
    }

    public void closeTelephone ()
    {
        telephone.SetActive(false);
    }

    public void openTelephone ()
    {
        foreach (Transform child in telephone.transform)
        {
            child.gameObject.SetActive(false); ;
        }
        telephone.transform.Find("Phone_Background").gameObject.SetActive(true);
        telephone.transform.Find("Phone_Apps").gameObject.SetActive(true);
        telephone.SetActive(true);
    }

    public bool canOpenSMS ()
    {
        return !sms.allConvAreDone();
    }

    public bool canOpenMessenger ()
    {
        return !messenger.allConvAreDone();
    }

    public bool canOpenCall()
    {
        return !talkedToGrandma;
    }

    public bool haveTalkedToGrandma ()
    {
        return talkedToGrandma;
    }

    public void setHaveTalkedToGrandmaTrue ()
    {
        talkedToGrandma = true;
    }

    public void quit ()
    {
        closeTelephone();
        credits.SetActive(true);
        AudioSource sound = credits.GetComponent<AudioSource>();
        sound.Play();
    }

    public bool PhoneIsOpen ()
    {
        return telephone.activeSelf;
    }

    public bool getAccessibilityAnim()
    {
        return accessibilityAnim;
    }

    public void setAccessibilityAnim (bool newA)
    {
        accessibilityAnim = newA;
    }

    public bool getAccessibilitySize()
    {
        return accessibilitySize;
    }

    public void setAccessibilitySize(bool newA)
    {
        accessibilitySize = newA;
    }

    public bool getAccessibilitySpeed()
    {
        return accessibilitySpeed;
    }

    public void setAccessibilitySpeed(bool newA)
    {
        accessibilitySpeed = newA;
    }
}
