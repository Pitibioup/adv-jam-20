﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Mail_Selection : MonoBehaviour
{
    public UnityEngine.UI.Button Bank_Button;
    public UnityEngine.UI.Button Government_Button;

    private GameManager gameManager;

    void Start()
    {
        gameManager = GameManager.Instance;

        Bank_Button.onClick.AddListener(() => gameManager.LancerAppliMails((int)0));
        Government_Button.onClick.AddListener(() => gameManager.LancerAppliMails((int)1));
    }
}
