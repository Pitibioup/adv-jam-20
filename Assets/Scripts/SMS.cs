﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Ink.Runtime;
using UnityEngine.UI;
using System;

public class SMS : MonoBehaviour
{
    // INSTANCE
    private static SMS _instance;
    public static SMS Instance { get { return _instance; } }

    // UNITY VARIABLES
    public GameObject typingBar;
    public GameObject typingText_Prefab;
    // prefabs
    public GameObject message_primary_prefab;
    public GameObject message_secondary_prefab;
    public GameObject choice_prefab;
    // parents
    public GameObject messages_parent;
    public GameObject choices_parent;
    // images
    public GameObject primary_image;
    public GameObject secondary_image;
    // pause
    private float pause_duration_sec;
    public float pause_duration_sec_accessibility;
    public float pause_duration_sec_base;
    public int end_pause_ms;
    // text
    public int nbCharInALine;

    // PRIVATE VARIABLES
    private Story story;
    private string[] noms;
    private bool[] recap;
    private Sprite primary_sprite;
    private Sprite secondary_sprite;
    private GameObject current_message_prefab;
    private bool choicesAlreadyDisplayed;
    private float timeToWait;
    private bool primary_talking;


    private void Awake()
    {
        // Singleton design pattern
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }

        noms = new string[] { "David", "Alex", "Sasha" };
        recap = new bool[] { false, false, false };
    }

    void Start()
    {
        primary_sprite = Resources.Load<Sprite>("Images/Player-avatar");
        primary_image.GetComponent<Image>().sprite = primary_sprite;

        current_message_prefab = message_primary_prefab;

        choicesAlreadyDisplayed = false;
        timeToWait = 0.0f;
        primary_talking = true;
    }

    void Update ()
    {
        if (story.canContinue)
        {
            if (timeToWait <= 0.0f )
            {
                if (NextMessage())
                {
                    choicesAlreadyDisplayed = false;
                }
            } else
            {
                timeToWait -= Time.deltaTime;
            }
        }
        else if (story.currentChoices.Count > 0)
        {
            if (!choicesAlreadyDisplayed)
            {
                displayChoices();
                choicesAlreadyDisplayed = true;
            }
        }
        else// END
        {
            endConversation();
        }
    }

    private bool NextMessage ()
    {
        string message_text = getNextMessage();
        if (messageNonVide(message_text)) // display message
        {
            if (primary_talking)
            {
                typeMessage(message_text);
                timeToWait = message_text.Length * 0.05f + 0.5f + pause_duration_sec;
            }
            else
            {
                displayNextMessage(message_text);
                timeToWait = pause_duration_sec;
            }
                return true;
        }
        return false;
    }

    public void displayNextMessage( string message_text)
    {
        GameObject message_obj = Instantiate(current_message_prefab, messages_parent.transform) as GameObject;
        message_obj.transform.Find("Text").gameObject.GetComponent<Text>().text = message_text;

        // Size
        int nb_lines = (int)Math.Ceiling(message_text.Length / ((float)nbCharInALine));
        float font_size = message_obj.transform.Find("Text").gameObject.GetComponent<Text>().fontSize;
        float interline = message_obj.transform.Find("Text").gameObject.GetComponent<Text>().lineSpacing;
        float height = nb_lines * font_size + (nb_lines + 1) * interline + 20;
        float width;
        if (nb_lines == 1)
        {
            int nbUp = nbUpperCharInText(message_text);
            width = (message_text.Length - nbUp) * font_size / 2 + nbUp * font_size + 10;
        }
        else
        {
            width = nbCharInALine * font_size / 2 + 15;
        }
        Vector2 message_size = new Vector2(width, height);
        message_obj.GetComponent<RectTransform>().sizeDelta = message_size;

        // Position
        float newX;
        if (primary_talking)
        { newX = -width / 2; }
        else
        { newX = width / 2; }
        MoveXPos(message_obj.GetComponent<RectTransform>(), newX);
        moveMessagesUp(height);
        if (primary_talking)
        {
            timeToWait = pause_duration_sec;
        }
    }

    private void typeMessage (string text)
    {
        GameObject obj = Instantiate(typingText_Prefab, typingBar.transform);
        obj.GetComponent<TMPro.TextMeshProUGUI>().text = text;
    }

    private string getNextMessage()
    {
        string message_text = story.Continue();
        if (message_text.StartsWith("[[1]]")) // message to display only if we haven't talked to Sacha
        {
            if (convIsDone(2)) // we've talked to Sacha -> do not display
            {
                message_text = "";
            }
            else // display
            {
                message_text = message_text.Remove(0, 5);
            }
        }
        if (message_text.StartsWith("[[2]]")) // message to display only if we have talked to Sacha
        {
            if (!convIsDone(2)) // we've not talked to Sacha -> do not display
            {
                message_text = "";
            }
            else // display
            {
                message_text = message_text.Remove(0, 5);
            }
        }
        if (message_text.StartsWith("(primary):")) // message sent by the player
        {
            message_text = message_text.Remove(0, 11);
            current_message_prefab = message_primary_prefab;
            primary_talking = true;
        }
        else if (message_text.StartsWith("(secondary):")) // message sent by secondary
        {
            message_text = message_text.Remove(0, 13);
            current_message_prefab = message_secondary_prefab;
            primary_talking = false;
        }
        return message_text;
    }

    private void displayChoices ()
    {
        Vector3 position;
        if (GameManager.Instance.getAccessibilitySize()) 
        {
            position = new Vector3(225.0f, 120.0f, 0.0f);
        } else
        {
            position = new Vector3(135.0f, 110.0f, 0.0f);
        }
        for (int i = 0; i < story.currentChoices.Count; i++)
        {
            GameObject choice = Instantiate(choice_prefab, position, choice_prefab.transform.rotation, choices_parent.transform) as GameObject;
            choice.transform.Find("Choice_Text").gameObject.GetComponent<Text>().text = story.currentChoices[i].text;
            choice.tag = i.ToString();
            position.x += choice.GetComponent<RectTransform>().sizeDelta.x + 20;
        }
    }

    public void endConversation()
    {
        System.Threading.Thread.Sleep(end_pause_ms);
        GameManager.Instance.FermerAppliSMS();
    }

    public void makeChoice (int choiceIndex)
    {
        current_message_prefab = message_primary_prefab;
        primary_talking = true;
        story.ChooseChoiceIndex(choiceIndex);
        destroyChoices();
        timeToWait = 0.0f;
    }

    /**
     * Method ouvrirAppli
     * ouvre une conversation avec l'interlocuteur donné
     * @param index l'index de l'interlocuteur
     */
    public void ouvrirAppli(int index)
    {
        string Interlocuteur = noms[index];
        TextAsset inkJson = Resources.Load<TextAsset>("Ink/SMS/" + Interlocuteur);
        story = new Story(inkJson.text);
        secondary_sprite = Resources.Load<Sprite>("Images/" + Interlocuteur + "-avatar");
        secondary_image.GetComponent<Image>().sprite = secondary_sprite;
        recap[index] = true;
        gameObject.SetActive(true);
        if (GameManager.Instance.getAccessibilitySpeed())
        {
            pause_duration_sec = pause_duration_sec_accessibility;
        } else
        {
            pause_duration_sec = pause_duration_sec_base;
        }
    }

    /**
     * Method fermerAppli
     */
    public void fermerAppli()
    {
        destroyMessages();
        gameObject.SetActive(false);
    }

    /**
     * Method convIsDone
     * @param name : le nom de l'interlocuteur
     * @return true si la conversation avec l'interlocuteur a déjà eu lieu
     */
    public bool convIsDone (int index)
    {
        return recap[index];
    }

    /**
     * Method allConvAreDone
     * @return true si toutes les conversations ont déjà eu lieu
     */
    public bool allConvAreDone ()
    {
        bool rep = true;
        for (int i = 0; i< recap.Length; i++)
        {
            if (!recap[i])
            {
                rep = false;
                break;
            }
        }
        return rep;
    }

    /**
     * Method destroyChoices
     * destroys all choices objects
     */
    private void destroyChoices ()
    {
        foreach (Transform child in choices_parent.transform)
        {
            Destroy(child.gameObject);
        }

    }

    /**
     * Method destroyMessages
     * destroys all messages objects
     */
    private void destroyMessages()
    {
        foreach (Transform child in messages_parent.transform)
        {
            Destroy(child.gameObject);
        }
    }

    /**
     * Method moveMessagesUp
     * moves all messages up
     * @param height the distance to move
     */
    private void moveMessagesUp(float height)
    {
        foreach (Transform child in messages_parent.transform)
        {
            float spacer = 20;
            SetYPos(child, child.position.y + height + spacer);
        }
    }

    /**
     * Method SetYPos
     * moves the transform to the new Y position
     * @param t the transform to move
     * @param newYPos the new position
     */
    private static void SetYPos(Transform t, float newYPos)
    {
        var pos = t.position;
        pos.y = newYPos;
        t.position = pos;
    }

    /**
     * Method MoveXPos
     * moves the transform of new X
     * @param t the transform to move
     * @param newYPos the new position
     */
    private static void MoveXPos(Transform t, float newXPos)
    {
        var pos = t.position;
        pos.x += newXPos;
        t.position = pos;
    }

    /**
     * Method messageNonVide
     * @param message_text the message to check
     * @return false si le message ne doit pas être affiché car vide
     */
    private bool messageNonVide(string message_text)
    {
        if (message_text.Length == 0 || String.Compare(message_text, Environment.NewLine) == 0 || String.Compare(message_text, " ") == 0)
        {
            return false;
        }
        return true;
    }

    private int nbUpperCharInText (string text)
    {
        int count = 0;
        for (int i = 0; i < text.Length; i++)
        {
            if (char.IsUpper(text[i])) count++;
        }
        return count;
    }
}
