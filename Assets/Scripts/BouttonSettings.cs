﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BouttonSettings : MonoBehaviour
{
    void Start()
    {
        GetComponent<UnityEngine.UI.Button>().onClick.AddListener(() => ButtonManager.Instance.fermerSettings());
    }

}