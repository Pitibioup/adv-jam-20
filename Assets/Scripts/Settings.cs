﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Settings : MonoBehaviour
{

    void Start()
    {
        transform.Find("ToggleAnimation").Find("On").gameObject.SetActive(false);
        transform.Find("ToggleAnimation").Find("Off").gameObject.SetActive(true);

        transform.Find("ToggleSize").Find("On").gameObject.SetActive(false);
        transform.Find("ToggleSize").Find("Off").gameObject.SetActive(true);

        transform.Find("ToggleSpeed").Find("On").gameObject.SetActive(false);
        transform.Find("ToggleSpeed").Find("Off").gameObject.SetActive(true);
    }

}