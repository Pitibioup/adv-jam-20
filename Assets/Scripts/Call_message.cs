﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Call_message : MonoBehaviour
{
    private bool is_primary;
    private float speed;
    private float accessibility = 150.0f;
    private float slow = 250.0f;
    private float fast = 350.0f;

    private float pic_size;
    private float width;
    private float parent_width;

    bool next_available;

    void Start()
    {
        Call call = Call.Instance;
        is_primary = String.Compare(gameObject.tag, "primary") == 0;
        pic_size = call.getPicSize();
        parent_width = call.getMessageParentSize();
        width = gameObject.GetComponent<RectTransform>().sizeDelta.x;
        speed = slow;

        next_available = true;
    }

    void Update()
    {
        if (string.Compare(gameObject.transform.Find("Text").gameObject.tag,"fast")==0) {
            speed = fast;
        }
        if (string.Compare(gameObject.transform.Find("Text").gameObject.tag, "accessibility") == 0)
        {
            speed = accessibility;
        }
        if (next_available && isTimeToShowNext())
        {
            Call.Instance.NextMessage();
            next_available = false;
        }else if (outOfRange())
        {
            Destroy(gameObject);
        }
        moveForwards();
    }

    private void moveForwards()
    {
        Vector3 newPosition = gameObject.GetComponent<RectTransform>().anchoredPosition;
        float step = speed * Time.deltaTime; // distance to move
        if (is_primary)
        {
            newPosition.x -= step;
        } else
        {
            newPosition.x += step;
        }
        gameObject.GetComponent<RectTransform>().anchoredPosition = newPosition;
    }

    private bool outOfRange()
    {
        float dist;
        if (is_primary)
        {
            dist = parent_width + gameObject.GetComponent<RectTransform>().anchoredPosition.x + width / 2 - pic_size;
        } else
        {
            dist = parent_width - gameObject.GetComponent<RectTransform>().anchoredPosition.x + width / 2 - pic_size;
        }
        bool ans = dist < 0.0f;
        return ans;
    }

    private bool isTimeToShowNext()
    {
        float dist;
        if (is_primary)
        {
            dist = parent_width/2 + gameObject.GetComponent<RectTransform>().anchoredPosition.x + width / 2 - pic_size;
        }
        else
        {
            dist = parent_width/2 - gameObject.GetComponent<RectTransform>().anchoredPosition.x + width / 2 - pic_size;
        }
        bool ans = dist < 0.0f;
        return ans;
    }
}
