﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Typing : MonoBehaviour
{
    private float lifeTime_sec;
    private string text;

    // Start is called before the first frame update
    void Start()
    {
        text = gameObject.GetComponent<TMPro.TextMeshProUGUI>().text;
        lifeTime_sec = text.Length * 0.05f + 0.5f;
    }

    // Update is called once per frame
    void Update()
    {
        if (lifeTime_sec > 0)
        {
            lifeTime_sec -= Time.deltaTime;
        } else
        {
            SMS.Instance.displayNextMessage(text);
            //TODO : add sound effect
            Destroy(gameObject);
        }
        
    }
}
