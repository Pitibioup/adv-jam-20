﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

public class ButtonManager : MonoBehaviour
{
    private static ButtonManager _instance;
    public static ButtonManager Instance { get { return _instance; } }

    public UnityEngine.UI.Button Call_Button;
    public UnityEngine.UI.Button SMS_Button;
    public UnityEngine.UI.Button Messenger_Button;
    public UnityEngine.UI.Button Album_Button;
    public UnityEngine.UI.Button Mails_Button;
    public UnityEngine.UI.Button Exit_Button;
    public UnityEngine.UI.Button Settings_Button;

    public GameObject phone_buttons;
    public GameObject SMS_Selection_Screen;
    public GameObject messenger_Selection_Screen;
    public GameObject mail_Selection_Screen;
    public GameObject picture_Selection_Screen;
    public GameObject settings;

    private GameManager gameManager;

    private void Awake()
    {
        // Singleton design pattern
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
        
    }

    void Start()
    {
        gameManager = GameManager.Instance;

        // True
        phone_buttons.SetActive(true);
        SMS_Selection_Screen.SetActive(true);
        messenger_Selection_Screen.SetActive(true);
        mail_Selection_Screen.SetActive(true);
        picture_Selection_Screen.SetActive(true);

        //Calls the right method when you click a button
        Call_Button.onClick.AddListener(() => select_Call_interlocuteur());
        SMS_Button.onClick.AddListener(() => select_SMS_interlocuteur());
        Messenger_Button.onClick.AddListener(() => select_Messenger_interlocuteur());
        Album_Button.onClick.AddListener(() => select_Album_picture());
        Mails_Button.onClick.AddListener(() => select_Mail_interlocuteur());
        Exit_Button.onClick.AddListener(() => gameManager.quit());
        Settings_Button.onClick.AddListener(() => openSettings());

        // False
        SMS_Selection_Screen.SetActive(false);
        messenger_Selection_Screen.SetActive(false);
        mail_Selection_Screen.SetActive(false);
        picture_Selection_Screen.SetActive(false);
        settings.SetActive(false);

        Vector3 pos = gameObject.GetComponent<RectTransform>().localPosition;
        pos.x = -0.25f * gameObject.transform.parent.gameObject.GetComponent<RectTransform>().sizeDelta.x;
        gameObject.GetComponent<RectTransform>().localPosition = pos;
    }

    private void select_Call_interlocuteur ()
    {
        if (!gameManager.haveTalkedToGrandma())
        {
            phone_buttons.SetActive(false);
            gameManager.setHaveTalkedToGrandmaTrue();
            gameManager.LancerAppliTelephone(0);
        }
        else
        {
            gameManager.nopeMessage("I have already called Grandma today.");
        }
    }

    private void select_SMS_interlocuteur()
    {
        if (gameManager.canOpenSMS())
        {
            phone_buttons.SetActive(false);
            SMS_Selection_Screen.SetActive(true);
        } else
        {
            gameManager.nopeMessage("I have already messaged everyone on here today.");
        }
        
    }

    private void select_Messenger_interlocuteur()
    {
        if (gameManager.canOpenMessenger())
        {
            phone_buttons.SetActive(false);
            messenger_Selection_Screen.SetActive(true);
        } else
        {
            gameManager.nopeMessage("I have already messaged everyone on here today.");
        }
        
    }

    private void select_Mail_interlocuteur()
    {
        phone_buttons.SetActive(false);
        mail_Selection_Screen.SetActive(true);
    }

    private void select_Album_picture()
    {
        phone_buttons.SetActive(false);
        picture_Selection_Screen.SetActive(true);
    }

    private void openSettings()
    {
        settings.SetActive(true);
        phone_buttons.SetActive(false);
    }

    public void fermerSettings()
    {
        settings.SetActive(false);
        phone_buttons.SetActive(true);
    }
}
