﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Messenger_Choice : MonoBehaviour
{
    void Start()
    {
        UnityEngine.UI.Button button = GetComponent<UnityEngine.UI.Button>();
        button.onClick.AddListener(() => Messenger.Instance.makeChoice(int.Parse(gameObject.tag)));
    }
}
