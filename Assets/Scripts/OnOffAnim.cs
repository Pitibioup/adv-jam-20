﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnOffAnim : MonoBehaviour
{
    private GameManager gameManager;
    private GameObject On;
    private GameObject Off;

    void Start()
    {
        On = transform.parent.Find("On").gameObject;
        Off = transform.parent.Find("Off").gameObject;

        gameManager = GameManager.Instance;

        GetComponent<UnityEngine.UI.Button>().onClick.AddListener(() => swapOnOff());
    }

    private void swapOnOff()
    {
        bool accessibility = !gameManager.getAccessibilityAnim();
        gameManager.setAccessibilityAnim(accessibility);
        On.SetActive(accessibility);
        Off.SetActive(!accessibility);
    }
}
