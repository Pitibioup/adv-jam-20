﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Call_choice : MonoBehaviour
{
    private int acessibilityFontSize = 30;

    void Awake()
    {
        if (GameManager.Instance.getAccessibilitySize())
        {
            transform.Find("Choice_Text").gameObject.GetComponent<UnityEngine.UI.Text>().fontSize = acessibilityFontSize;
            Vector3 size = GetComponent<RectTransform>().sizeDelta;
            size.y += 10.0f;
            size.x += 150.0f;
            GetComponent<RectTransform>().sizeDelta = size;
        }
    }

    void Start()
    {
        UnityEngine.UI.Button button = GetComponent<UnityEngine.UI.Button>();
        button.onClick.AddListener(() => Call.Instance.makeChoice(int.Parse(gameObject.tag)));
    }
}
