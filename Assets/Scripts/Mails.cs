﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Ink.Runtime;
using UnityEngine.UI;
using System;

public class Mails : MonoBehaviour
{
    private static Mails _instance;
    public static Mails Instance { get { return _instance; } }

    public UnityEngine.UI.Button Close_Button;
    public GameObject mail_field;

    private string[] noms;

    private void Awake()
    {
        // Singleton design pattern
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
        noms = new string[] { "Bank", "Government" };
    }

    void Start()
    {
        Close_Button.onClick.AddListener(() => GameManager.Instance.FermerAppliMails());
    }

    public void ouvrirAppli(int index)
    {
        Sprite sprite = Resources.Load<Sprite>("Mails/"+noms[index]);
        mail_field.GetComponent<Image>().sprite = sprite;
        gameObject.SetActive(true);
    }


    public void fermerAppli()
    {
        gameObject.SetActive(false);
    }
}
