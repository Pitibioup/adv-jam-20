﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Ink.Runtime;
using UnityEngine.UI;
using System;
public class Album : MonoBehaviour
{
    private static Album _instance;
    public static Album Instance { get { return _instance; } }

    public UnityEngine.UI.Button Close_Button;
    public GameObject picture_field;

    private void Awake()
    {
        // Singleton design pattern
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    void Start ()
    {
        Close_Button.onClick.AddListener(() => GameManager.Instance.FermerAppliAlbum());
    }

    public void ouvrirAppli(int index)
    {
        Sprite sprite = Resources.Load<Sprite>("Album/" + index.ToString());
        picture_field.GetComponent<Image>().sprite = sprite;
        gameObject.SetActive(true);
    }


    public void fermerAppli()
    {
        gameObject.SetActive(false);
    }
}
