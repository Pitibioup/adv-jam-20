﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Ink.Runtime;
using UnityEngine.UI;
using System;


public class Call : MonoBehaviour
{
    private static Call _instance;
    public static Call Instance { get { return _instance; } }

    // UNITY VARIABLES
    public GameObject destroyMessage;
    // prefabs
    public GameObject message_primary_prefab;
    public GameObject message_secondary_prefab;
    public GameObject choice_prefab;
    // parents
    public GameObject messages_parent;
    public GameObject choices_parent;
    // images
    public GameObject primary_image;
    public GameObject secondary_image;
    // pause
    public int end_pause_ms;

    // PRIVATE VARIABLES
    private Story story;
    private string[] noms;
    private bool[] recap;
    private Sprite primary_sprite;
    private Sprite secondary_sprite;
    private bool choicesAlreadyDisplayed;
    private GameObject current_message_prefab;
    private bool primary_is_talking;
    private bool fast;
    private float fast_speed;
    
    private void Awake()
    {
        // Singleton design pattern
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }

        noms = new string[] { "Grandma" };
        recap = new bool[] { false };
        destroyMessage.SetActive(false);
    }

    void Start()
    {
        primary_sprite = Resources.Load<Sprite>("Images/Player-avatar");
        primary_image.GetComponent<Image>().sprite = primary_sprite;

        choicesAlreadyDisplayed = false;
    }

    void Update ()
    {
        if (story.canContinue)
        {
        }
        else if (story.currentChoices.Count > 0)
        {
            if (!choicesAlreadyDisplayed)
            {
                displayChoices();
                choicesAlreadyDisplayed = true;
            }
        }
        else// END
        {
            endConversation();
        }
    }

    public bool NextMessage()
    {
        if (story.canContinue)
        {
            string message_text = getNextMessage();
            if (messageNonVide(message_text))
            {
                if (!message_text.StartsWith("[[destroy]]"))
                {
                    displayNextMessage(message_text);
                }
                return true;
            } else
            {
                return (NextMessage());
            }
        } else
        {
            return false;
        }
    }

    public void displayNextMessage(string message_text)
    {
        GameObject message_obj = Instantiate(current_message_prefab, messages_parent.transform) as GameObject;
        message_obj.transform.Find("Text").gameObject.GetComponent<TMPro.TextMeshProUGUI>().text = message_text;

        if (fast)
        {
            message_obj.transform.Find("Text").gameObject.tag = "fast";
        }
        // Size
        float font_size = message_obj.transform.Find("Text").gameObject.GetComponent<TMPro.TextMeshProUGUI>().fontSize;
        float interline = message_obj.transform.Find("Text").gameObject.GetComponent<TMPro.TextMeshProUGUI>().lineSpacing;
        float height = font_size + 20;
        float width;
        int nbUp = nbUpperCharInText(message_text);
        width = (message_text.Length - nbUp) * font_size / 2 + nbUp * font_size + 10;
        Vector2 message_size = new Vector2(width, height);
        message_obj.GetComponent<RectTransform>().sizeDelta = message_size;

        // Position
        float newX = width/2.0f - getPicSize();
        if (!primary_is_talking)
        {
            newX = -newX;
        }
        SetXPos(message_obj, newX);
    }

    private string getNextMessage()
    {
        string message_text = story.Continue();
        fast = false;
        if (message_text.StartsWith("[[fast]]"))
        {
            fast = true;
            message_text = message_text.Remove(0, 8);
        } else if (message_text.StartsWith("[[destroy]]"))
        {
            displayDestroy();
            return message_text;
        }

        // normal case;
            if (message_text.StartsWith("(primary):")) // message sent by the player
        {
            message_text = message_text.Remove(0, 11);
            current_message_prefab = message_primary_prefab;
            primary_is_talking = true;
        }
        else if (message_text.StartsWith("(secondary):")) // message sent by secondary
        {
            message_text = message_text.Remove(0, 13);
            current_message_prefab = message_secondary_prefab;
            primary_is_talking = false;
        }
        return message_text;
    }

    private void displayDestroy ()
    {
        destroyMessage.SetActive(true);
    }

    private void displayChoices()
    {
        Vector3 position = new Vector3(-300.0f, 95.0f, 0.0f);
        for (int i = 0; i < story.currentChoices.Count; i++)
        {
            GameObject choice = Instantiate(choice_prefab, position, choice_prefab.transform.rotation, choices_parent.transform) as GameObject;
            choice.transform.Find("Choice_Text").gameObject.GetComponent<Text>().text = story.currentChoices[i].text;
            choice.tag = i.ToString();
            SetXPos(choice, position.x);
            position.y += choice.GetComponent<RectTransform>().sizeDelta.y + 20;
        }
    }

    public void endConversation()
    {
        System.Threading.Thread.Sleep(end_pause_ms);
        GameManager.Instance.FermerAppliTelephone();
    }

    public void makeChoice(int choiceIndex)
    {
        story.ChooseChoiceIndex(choiceIndex);
        current_message_prefab = message_primary_prefab;
        destroyChoices();
        choicesAlreadyDisplayed = false;
        primary_is_talking = true;
        NextMessage();
    }

    /**
     * Method ouvrirAppli
     * ouvre une conversation avec l'interlocuteur donné
     * @param index l'index de l'interlocuteur
     */
    public void ouvrirAppli(int index)
    {
        string Interlocuteur = noms[index];
        TextAsset inkJson = Resources.Load<TextAsset>("Ink/Call/" + Interlocuteur);
        story = new Story(inkJson.text);
        secondary_sprite = Resources.Load<Sprite>("Images/" + Interlocuteur + "-avatar");
        secondary_image.GetComponent<Image>().sprite = secondary_sprite;
        recap[index] = true;
        gameObject.SetActive(true);
        NextMessage();
    }

    /**
     * Method fermerAppli
     */
    public void fermerAppli()
    {
        destroyMessages();
        gameObject.SetActive(false);
    }

    /**
     * Method convIsDone
     * @param name : le nom de l'interlocuteur
     * @return true si la conversation avec l'interlocuteur a déjà eu lieu
     */
    public bool convIsDone(int index)
    {
        return recap[index];
    }

    /**
     * Method allConvAreDone
     * @return true si toutes les conversations ont déjà eu lieu
     */
    public bool allConvAreDone()
    {
        bool rep = true;
        for (int i = 0; i < recap.Length; i++)
        {
            if (!recap[i])
            {
                rep = false;
                break;
            }
        }
        return rep;
    }

    /**
     * Method destroyChoices
     * destroys all choices objects
     */
    private void destroyChoices()
    {
        foreach (Transform child in choices_parent.transform)
        {
            Destroy(child.gameObject);
        }

    }

    /**
     * Method destroyMessages
     * destroys all messages objects
     */
    private void destroyMessages()
    {
        foreach (Transform child in messages_parent.transform)
        {
            Destroy(child.gameObject);
        }
    }

    /**
     * Method SetYPos
     * moves the transform to the new Y position
     * @param t the transform to move
     * @param newYPos the new position
     */
    private static void SetYPos(Transform t, float newYPos)
    {
        var pos = t.position;
        pos.y = newYPos;
        t.position = pos;
    }

    /**
     * Method SetXPos
     * moves the gameObject's anchored position to new X
     * @param t the transform to move
     * @param newYPos the new position
     */
    private static void SetXPos(GameObject obj, float newXPos)
    {
        var pos = obj.GetComponent<RectTransform>().anchoredPosition;
        pos.x = newXPos;
        obj.GetComponent<RectTransform>().anchoredPosition = pos;
    }

    /**
     * Method messageNonVide
     * @param message_text the message to check
     * @return false si le message ne doit pas être affiché car vide
     */
    private bool messageNonVide(string message_text)
    {
        if (message_text.Length == 0 || String.Compare(message_text, Environment.NewLine) == 0 || String.Compare(message_text, " ") == 0)
        {
            return false;
        }
        return true;
    }

    private int nbUpperCharInText(string text)
    {
        int count = 0;
        for (int i = 0; i < text.Length; i++)
        {
            if (char.IsUpper(text[i])) count++;
        }
        return count;
    }

    public float getPicSize ()
    {
        float rep = primary_image.GetComponent<RectTransform>().sizeDelta.x;
        return rep;
    }

    public float getMessageParentSize()
    {
        float rep = transform.parent.gameObject.GetComponent<RectTransform>().sizeDelta.x;
        return rep;
    }
}
