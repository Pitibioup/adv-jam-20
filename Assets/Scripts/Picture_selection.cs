﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Picture_selection : MonoBehaviour
{
    public UnityEngine.UI.Button Button_0;
    public UnityEngine.UI.Button Button_1;
    public UnityEngine.UI.Button Button_2;
    public UnityEngine.UI.Button Button_3;
    public UnityEngine.UI.Button Button_4;
    public UnityEngine.UI.Button Button_5;

    private GameManager gameManager;

    void Start()
    {
        gameManager = GameManager.Instance;

        Button_0.onClick.AddListener(() => gameManager.LancerAppliAlbum((int)0));
        Button_1.onClick.AddListener(() => gameManager.LancerAppliAlbum((int)1));
        Button_2.onClick.AddListener(() => gameManager.LancerAppliAlbum((int)2));
        Button_3.onClick.AddListener(() => gameManager.LancerAppliAlbum((int)3));
        Button_4.onClick.AddListener(() => gameManager.LancerAppliAlbum((int)4));
        Button_5.onClick.AddListener(() => gameManager.LancerAppliAlbum((int)5));

    }
}
