﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SMSAccessibility : MonoBehaviour
{
    private int acessibilityFontSize = 37;

    void Awake()
    {
        if (GameManager.Instance.getAccessibilitySize())
        {
            transform.Find("Text").gameObject.GetComponent<UnityEngine.UI.Text>().fontSize = acessibilityFontSize;
        }
    }
}
