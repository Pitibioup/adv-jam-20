﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SMS_Selection : MonoBehaviour
{
    public UnityEngine.UI.Button David_Button;
    public UnityEngine.UI.Button Alex_Button;
    public UnityEngine.UI.Button Sacha_Button;

    private GameManager gameManager;

    void Start()
    {
        gameManager = GameManager.Instance;

        David_Button.onClick.AddListener(() => gameManager.LancerAppliSMS((int)0));
        Alex_Button.onClick.AddListener(() => gameManager.LancerAppliSMS((int)1));
        Sacha_Button.onClick.AddListener(() => gameManager.LancerAppliSMS((int)2));
    }
}
