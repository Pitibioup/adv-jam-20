﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class explosion : MonoBehaviour
{
    private float lifeTime = 0.06f;
    private int nb_total = 26;

    // Var
    private float remainingLifeTime;
    private int index;

    // Start is called before the first frame update
    void Start()
    {
        index = 0;
        changeImage(false);
    }

    // Update is called once per frame
    void Update()
    {
        remainingLifeTime -= Time.deltaTime;
        if (remainingLifeTime <= 0)
        {
            changeImage();
        }
    }

    private String getImageString (int index)
    {
        String ans = "Explosion_" + index.ToString();
        return ans;
    }

    private void changeImage(bool setUnactive = true)
    {
        if (setUnactive)
        {
            transform.Find(getImageString(index)).gameObject.SetActive(false);
        }
        index++;
        remainingLifeTime = lifeTime;
        if (index <= nb_total)
        {
            transform.Find(getImageString(index)).gameObject.SetActive(true);
        } else
        {
            Destroy(gameObject);
        }
    }
}
