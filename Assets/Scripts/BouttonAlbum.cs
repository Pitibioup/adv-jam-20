﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BouttonAlbum : MonoBehaviour
{
    void Start()
    {
        GetComponent<UnityEngine.UI.Button>().onClick.AddListener(() => GameManager.Instance.FermerAppliAlbum());
    }
}
