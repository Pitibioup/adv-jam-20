﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Ink.Runtime;
using UnityEngine.UI;
using System;
using System.Runtime.InteropServices;

public class Messenger : MonoBehaviour
{
    private static Messenger _instance;
    public static Messenger Instance { get { return _instance; } }

    // UNITY VARIABLES
    // prefabs
    public GameObject message_primary_prefab;
    public GameObject message_secondary_prefab;
    public GameObject choice_prefab;
    // parents
    public GameObject messages_parent;
    public GameObject choices_parent;
    // images
    public GameObject primary_image;
    public GameObject secondary_image;
    // pause
    public float pause_duration_sec_accessibility;
    public float pause_duration_sec_base;
    private float pause_duration_sec;
    public int end_pause_ms;

    // PRIVATE VARIABLES
    private Story story;
    private string[] noms;
    private bool[] recap;
    private Sprite primary_sprite;
    private Sprite secondary_sprite;
    private bool choicesAlreadyDisplayed;
    private float timeToWait;
    private float emojiHeight;
    private GameObject current_message_prefab;

    private void Awake()
    {
        // Singleton design pattern
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }

        noms = new string[] { "Sister", "Karim" };
        recap = new bool[] { false, false };
    }

    void Start ()
    {
        primary_sprite = Resources.Load<Sprite>("Images/Player-avatar");
        primary_image.GetComponent<Image>().sprite = primary_sprite;

        choicesAlreadyDisplayed = false;
        timeToWait = 0.0f;
        emojiHeight = 150; // TODO : initialisaiton en fonction de la taille du canvas
    }

    void Update ()
    {
        if (story.canContinue)
        {
            if (timeToWait <= 0.0f)
            {
                if (NextMessage())
                {
                    choicesAlreadyDisplayed = false;
                    timeToWait = pause_duration_sec;
                }
            }
            else
            {
                timeToWait -= Time.deltaTime;
            }
        }
        else if (story.currentChoices.Count > 0)
        {
            if (!choicesAlreadyDisplayed)
            {
                displayChoices();
                choicesAlreadyDisplayed = true;
            }
        }
        else// END
        {
            endConversation();
        }
    }

    private bool NextMessage()
    {
        string message_text = getNextMessage();
        if (messageNonVide(message_text)) // display message
        {
            displayNextMessage(message_text);
            return true;
        }
        return false;
    }

    public void displayNextMessage(string message_text)
    {
        GameObject message_obj = Instantiate(current_message_prefab, messages_parent.transform) as GameObject;
        Sprite sprite = Resources.Load<Sprite>("Emojis/" + message_text);
        message_obj.GetComponent<Image>().sprite = sprite;
        //TODO : set size
        moveMessagesUp(emojiHeight);
    }

    private string getNextMessage()
    {
        string message_text = story.Continue();
        if (message_text.StartsWith("[[1]]")|| message_text.StartsWith("$$1$$")) // message to display only if we haven't talked to grandma
        {
            if (!GameManager.Instance.haveTalkedToGrandma()) // we've talked to grandma -> do not display
            {
                message_text = "";
            }
            else // display
            {
                message_text = message_text.Remove(0, 5);
            }
        }
        if (message_text.StartsWith("(primary):")) // message sent by the player
        {
            message_text = message_text.Remove(0, 11);
            current_message_prefab = message_primary_prefab;
        }
        else if (message_text.StartsWith("(secondary):")) // message sent by secondary
        {
            message_text = message_text.Remove(0, 13);
            current_message_prefab = message_secondary_prefab;
        }
        if (message_text.Length >= 1 && message_text.EndsWith("\n"))
        {
            message_text = message_text.Remove(message_text.Length - 1, 1);
        }
        return message_text;
    }

    private void displayChoices()
    {
        if (story.currentChoices[0].text.StartsWith("$$1$$")&& !GameManager.Instance.haveTalkedToGrandma())
        {
            makeChoice(0);
        } 
        Vector3 position = new Vector3(300.0f, 95.0f, 0.0f);
        for (int i = 0; i < story.currentChoices.Count; i++)
        {
            string choice_text = story.currentChoices[i].text;
            if (choice_text.StartsWith("$$1$$")) // message to display only if we have talked to grandma
            {
                if (!GameManager.Instance.haveTalkedToGrandma()) // we've talked to grandma -> do not display
                {
                    break;
                }
                else // display
                {
                    choice_text = choice_text.Remove(0, 5);
                }
            }
            GameObject choice = Instantiate(choice_prefab, position, choice_prefab.transform.rotation, choices_parent.transform) as GameObject;
            Sprite sprite = Resources.Load<Sprite>("Emojis/" + choice_text);
            choice.GetComponent<Image>().sprite = sprite;
            choice.tag = i.ToString();
            position.x += choice.GetComponent<RectTransform>().sizeDelta.x + 20;
        }
    }

    public void endConversation()
    {
        System.Threading.Thread.Sleep(end_pause_ms);
        GameManager.Instance.FermerAppliMessenger();
    }

    public void makeChoice(int choiceIndex)
    {
        current_message_prefab = message_primary_prefab;
        story.ChooseChoiceIndex(choiceIndex);
        destroyChoices();
        timeToWait = 0.0f;
    }

    /**
     * Method ouvrirAppli
     * ouvre une conversation avec l'interlocuteur donné
     * @param index l'index de l'interlocuteur
     */
        public void ouvrirAppli(int index)
    {
        string Interlocuteur = noms[index];
        TextAsset inkJson = Resources.Load<TextAsset>("Ink/Messenger/" + Interlocuteur);
        story = new Story(inkJson.text);
        secondary_sprite = Resources.Load<Sprite>("Images/" + Interlocuteur + "-avatar");
        secondary_image.GetComponent<Image>().sprite = secondary_sprite;
        recap[index] = true;
        gameObject.SetActive(true);
        if (GameManager.Instance.getAccessibilitySpeed())
        {
            pause_duration_sec = pause_duration_sec_accessibility;
        } else
        {
            pause_duration_sec = pause_duration_sec_base;
        }
        
    }

    /**
     * Method fermerAppli
     */
    public void fermerAppli()
    {
        destroyMessages();
        gameObject.SetActive(false);
    }

    /**
     * Method convIsDone
     * @param name : le nom de l'interlocuteur
     * @return true si la conversation avec l'interlocuteur a déjà eu lieu
     */
    public bool convIsDone(int index)
    {
        return recap[index];
    }

    /**
     * Method allConvAreDone
     * @return true si toutes les conversations ont déjà eu lieu
     */
    public bool allConvAreDone()
    {
        bool rep = true;
        for (int i = 0; i < recap.Length; i++)
        {
            if (!recap[i])
            {
                rep = false;
                break;
            }
        }
        return rep;
    }

    /**
     * Method destroyChoices
     * destroys all choices objects
     */
    private void destroyChoices()
    {
        foreach (Transform child in choices_parent.transform)
        {
            Destroy(child.gameObject);
        }

    }

    /**
     * Method destroyMessages
     * destroys all messages objects
     */
    private void destroyMessages()
    {
        foreach (Transform child in messages_parent.transform)
        {
            Destroy(child.gameObject);
        }
    }

    /**
     * Method moveMessagesUp
     * moves all messages up
     * @param height the distance to move
     */
    private void moveMessagesUp(float height)
    {
        foreach (Transform child in messages_parent.transform)
        {
            float spacer = 20;
            SetYPos(child, child.position.y + height + spacer);
        }
    }

    /**
     * Method SetYPos
     * moves the transform to the new Y position
     * @param t the transform to move
     * @param newYPos the new position
     */
    private static void SetYPos(Transform t, float newYPos)
    {
        var pos = t.position;
        pos.y = newYPos;
        t.position = pos;
    }

    /**
     * Method messageNonVide
     * @param message_text the message to check
     * @return false si le message ne doit pas être affiché car vide
     */
    private bool messageNonVide(string message_text)
    {
        if (message_text.Length == 0 || String.Compare(message_text, "\n") == 0 || String.Compare(message_text, " ") == 0)
        {
            return false;
        }
        return true;
    }
}
