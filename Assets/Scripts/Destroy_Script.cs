﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Destroy_Script : MonoBehaviour
{
    private bool is_primary;
    private float speed = 350.0f;

    private float pic_size;
    private float width;
    private float parent_width;

    void Start()
    {
        Call call = Call.Instance;
        is_primary = String.Compare(gameObject.tag, "primary") == 0;
        pic_size = call.getPicSize();
        parent_width = call.getMessageParentSize();
        width = gameObject.GetComponent<RectTransform>().sizeDelta.x;
    }

    void Update()
    {
        if (outOfRange())
        {
            if (is_primary)
            {
                Call.Instance.NextMessage();
            } else
            {
                transform.parent.Find("Explosion").gameObject.SetActive(true);
            }
            Destroy(gameObject);
        }
        moveForwards();
    }

    private void moveForwards()
    {
        Vector3 newPosition = gameObject.GetComponent<RectTransform>().anchoredPosition;
        float step = speed * Time.deltaTime; // distance to move
        if (is_primary)
        {
            newPosition.x -= step;
        }
        else
        {
            newPosition.x += step;
        }
        gameObject.GetComponent<RectTransform>().anchoredPosition = newPosition;
    }

    private bool outOfRange()
    {
        float dist;
        if (is_primary)
        {
            dist = parent_width + gameObject.GetComponent<RectTransform>().anchoredPosition.x + width / 2 - pic_size;
        }
        else
        {
            dist = parent_width/1.6f - gameObject.GetComponent<RectTransform>().anchoredPosition.x + width / 2 - pic_size;
        }
        bool ans = dist < 0.0f;
        return ans;
    }
}
